package sundeepk.github.com.sample;

import com.github.sundeepk.compactcalendarview.EventsContainer;
import com.github.sundeepk.compactcalendarview.LogUtil;
import com.github.sundeepk.compactcalendarview.WeekUtils;
import com.github.sundeepk.compactcalendarview.domain.Event;
import org.junit.Test;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * ExampleTest
 *
 * @author hal
 * @since 2021/06/21
 */
public class ExampleTest {
    /**
     * onStart
     */
    @Test
    public void onStart() {
    }

    /**
     * testBundleWeekdayNames
     */
    @Test
    public void testBundleWeekdayNames() {
        String[] weekdayNames = WeekUtils.getWeekdayNames(Locale.getDefault(),
                Calendar.getInstance().getFirstDayOfWeek(), false);
        assertEquals("星", weekdayNames[0]);
        LogUtil.loge(weekdayNames[0]);
    }

    /**
     * testBundleAddEvent
     */
    @Test
    public void testBundleAddEvent() {
        EventsContainer eventsContainer = new EventsContainer(
                Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault()));
        long timeMillis = System.currentTimeMillis();
        Event event = new Event(-1, timeMillis);
        eventsContainer.addEvent(event);
        assertNotNull(eventsContainer.getEventsFor(timeMillis));
        LogUtil.loge("testBundleAddEvent =" + (eventsContainer.getEventsFor(timeMillis) != null));
    }

    /**
     * testBundleGetEvent
     */
    @Test
    public void testBundleGetEvent() {
        EventsContainer eventsContainer = new EventsContainer(
                Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault()));
        List<Event> events = eventsContainer.getEventsFor(System.currentTimeMillis());
        assertNotNull(events);
        LogUtil.loge("testBundleGetEvent =" + events.size());
    }
}
