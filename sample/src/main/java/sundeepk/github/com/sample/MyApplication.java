package sundeepk.github.com.sample;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @author hal
 * @since 2021/06/21
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
