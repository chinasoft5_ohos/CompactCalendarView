/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;

/**
 * IndicatorStyle
 *
 * @Author: AriesHoo on 2018/12/3 11:52
 * @E-Mail: AriesHoo@126.com
 * @Function: 指示器样式
 * @Description: IndicatorStyle
 * @since 2021/06/21
 */
public class IndicatorStyle {
    /**
     * 常规-直线
     */
    public static final int NORMAL = 0;

    /**
     * 三角形
     */
    public static final int TRIANGLE = 1;
    /**
     * 背景色块
     */
    public static final int BLOCK = 2;
}
