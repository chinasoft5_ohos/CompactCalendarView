package sundeepk.github.com.sample;


import com.github.sundeepk.compactcalendarview.FindUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Rect;
import ohos.agp.utils.TextTool;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.Locale;


/**
 * tab
 *
 * @Author: AriesHoo on 2018/11/30 11:18
 * @E-Mail: AriesHoo@126.com
 * @Function: 滑动TabLayout, 对于ViewPager的依赖性强
 * @Description: 1、2018-11-30 11:18:41 修改原库
 * https://github.com/H07000223/FlycoTabLayout 选中粗体当初始化选中第一项不生效BUG
 * {@link #updateTabStyles()}
 * 2、2018-12-4 14:12:34 新增部分设置方法返回值方便链式调用;删除原库相应xml属性 set/get方法
 * 3、2018-12-13 09:40:51 新增选中文字字号设置 textSelectSize
 * 5、2020-3-2 17:29:40 新增变量以修复点击及二次点击回调错误问题{@link #setCurrentTab(int)}
 * @since 2021/06/21
 */
public class SlidingTabLayout extends ScrollView implements
        ITabLayout, Component.DrawTask, PageSlider.PageChangedListener,
        Component.TouchEventListener {
    private static final int STYLE_NORMAL = 0;

    private static final int STYLE_TRIANGLE = 1;

    private static final int STYLE_BLOCK = 2;

    private TabSlidingDelegate mDelegate;
    private Context mContext;
    private PageSlider mViewPager;
    private DirectionalLayout mTabsContainer;
    private int mCurrentSelected = -1;
    private int mCurrentTab;
    private float mCurrentPositionOffset;
    private int mTabCount;
    /**
     * 用于实现滚动居中
     */
    private final Rect mTabRect = new Rect();
    /**
     * 用于绘制显示器
     */
    private final Rect mIndicatorRect = new Rect();
    private final ShapeElement mIndicatorDrawable = new ShapeElement();

    private final Paint mRectPaint = new Paint();
    private final Paint mDividerPaint = new Paint();
    private final Paint mTrianglePaint = new Paint();
    private final Path mTrianglePath = new Path();

    private int mLastScrollX;
    private int scrollOffset;
    /**
     * 第一次创建 解决首次不滑动问题
     */
    private boolean isFirstCreate = true;
    private int size;
    private double downX;

    public SlidingTabLayout(Context context) {
        this(context, null);
    }

    public SlidingTabLayout(Context context, AttrSet attrs) {
        super(context, attrs);
        if (attrs == null) {
            return;
        }
        mDelegate = new TabSlidingDelegate(this, attrs, this);
        this.mContext = context;
        mTabsContainer = new DirectionalLayout(context);
        // get layout_height
        String height = attrs.getAttr("height").get().getStringValue();
        // create ViewPager
        DirectionalLayout.LayoutConfig layoutConfig;
        if (height.equals(ComponentContainer.LayoutConfig.MATCH_PARENT + "")) {
            layoutConfig = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                    LayoutConfig.MATCH_PARENT);
        } else if (height.equals(ComponentContainer.LayoutConfig.MATCH_CONTENT + "")) {
            layoutConfig = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                    LayoutConfig.MATCH_CONTENT);
        } else {
            layoutConfig = new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                    LayoutConfig.MATCH_PARENT);
        }
        mTabsContainer.setOrientation(DirectionalLayout.HORIZONTAL);
        mTabsContainer.setLayoutConfig(layoutConfig);
        addComponent(mTabsContainer);
        addDrawTask(this, DrawTask.BETWEEN_BACKGROUND_AND_CONTENT);
        // scroll不会重绘 手动重绘
        addScrolledListener((component, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            scrollOffset += (oldScrollX - scrollX);
            mIndicatorRect.left = mIndicatorRect.left - (scrollX - oldScrollX);
            mIndicatorRect.right = mIndicatorRect.right - (scrollX - oldScrollX);
            invalidate();
        });
    }


    /**
     * getDelegate
     *
     * @return mDelegate
     */
    public TabSlidingDelegate getDelegate() {
        return mDelegate;
    }

    /**
     * 关联ViewPager
     *
     * @param vp viewpager
     * @throws IllegalStateException PageSlider为null的时候
     */
    public void setViewPager(PageSlider vp) {
        if (vp == null || vp.getProvider() == null) {
            throw new IllegalStateException("ViewPager or ViewPager adapter can not be NULL !");
        }
        this.mViewPager = vp;
        try {
            this.mViewPager.removePageChangedListener(this);
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        this.mViewPager.addPageChangedListener(this);
        this.mViewPager.setTouchEventListener(this);
        notifyDataSetChanged();
    }


    /**
     * 更新数据
     */
    public void notifyDataSetChanged() {
        mTabsContainer.removeAllComponents();
        this.mTabCount = mViewPager.getProvider().getCount();
        Component tabView;
        for (int i = 0; i < mTabCount; i++) {
            tabView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_layout_tab, null, false);
            CharSequence pageTitle = mViewPager.getProvider().getPageTitle(i);
            addTab(i, TextTool.isNullOrEmpty(pageTitle) ? "" : pageTitle.toString(), tabView);
        }
        if (mCurrentTab != mViewPager.getCurrentPage()) {
            setCurrentTab(mViewPager.getCurrentPage());
        }
        updateTabStyles();
    }

    private void addTab(final int position, String title, Component tabView) {
        Text tvTabTitle = FindUtils.findTextById(tabView, ResourceTable.Id_tv_tab_title);
        if (tvTabTitle != null && title != null) {
            tvTabTitle.setText(title);
        }
        tabView.setClickedListener(component -> {
            int position1 = mTabsContainer.getChildIndex(component);
            setCurrentTab(position1);

        });
        DirectionalLayout.LayoutConfig lpTab = getDelegate().isTabSpaceEqual() ?
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                        DirectionalLayout.LayoutConfig.MATCH_PARENT, LayoutAlignment.CENTER, 1.0f) :
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_PARENT);
        if (getDelegate().getTabWidth() > 0) {
            lpTab = new DirectionalLayout.LayoutConfig(getDelegate().getTabWidth(),
                    DirectionalLayout.LayoutConfig.MATCH_PARENT);
        }
        mTabsContainer.addComponent(tabView, position, lpTab);
    }

    @Override
    public void updateTabStyles() {
        for (int i = 0; i < mTabCount; i++) {
            Component component = mTabsContainer.getComponentAt(i);
            Text tvTabTitle = FindUtils.findTextById(component, ResourceTable.Id_tv_tab_title);
            if (tvTabTitle != null) {
                tvTabTitle.setTextColor(new Color(i == mCurrentTab ?
                        getDelegate().getTextSelectColor() :
                        getDelegate().getTextUnSelectColor()));
                tvTabTitle.setTextSize((int) (mCurrentTab == i ?
                                getDelegate().getTextSelectSize() : getDelegate().getTextSize()),
                        getDelegate().getTextSizeUnit());
                tvTabTitle.setPadding(getDelegate().getTabPadding(),
                        0, getDelegate().getTabPadding(), 0);
                if (getDelegate().isTextAllCaps()) {
                    tvTabTitle.setText(tvTabTitle.getText().toUpperCase(Locale.ROOT));
                }
            }
        }
    }

    @Override
    public ComponentContainer.LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
        return super.createLayoutConfig(context, attrSet);
    }

    /**
     * HorizontalScrollView滚到当前tab,并且居中显示
     */
    private void scrollToCurrentTab() {
        if (mTabCount <= 0) {
            return;
        }
        int offset = (int) (mCurrentPositionOffset * mTabsContainer.getComponentAt(mCurrentTab)
                .getWidth());
        // 当前Tab的left+当前Tab的Width乘以positionOffset
        int newScrollX = mTabsContainer.getComponentAt(mCurrentTab).getLeft() + offset;

        if (mCurrentTab > 0 || offset > 0) {
            // HorizontalScrollView移动到当前tab,并居中
            newScrollX -= getWidth() / 2 - getPaddingLeft();
            calcIndicatorRect();
            newScrollX += ((mTabRect.right - mTabRect.left) / 2);
        }
        if (newScrollX != mLastScrollX) {
            mLastScrollX = newScrollX;
            /*
             * scrollTo（int x,int y）:x,y代表的不是坐标点,而是偏移量
             *  x:表示离起始位置的x水平方向的偏移量
             *  y:表示离起始位置的y垂直方向的偏移量
             */
            fluentScrollTo(newScrollX, 0);
        }
    }

    private void updateTabSelection(int position) {
        for (int i = 0; i < mTabCount; ++i) {
            Component tabView = mTabsContainer.getComponentAt(i);
            final boolean isSelect = i == position;
            Text tabTitle
                    = FindUtils.findTextById(tabView, ResourceTable.Id_tv_tab_title);

            if (tabTitle != null) {
                tabTitle.setTextColor(new Color(isSelect ? getDelegate().getTextSelectColor()
                        : getDelegate().getTextUnSelectColor()));
                tabTitle.setTextSize((int) (mCurrentTab == i ? getDelegate().getTextSelectSize()
                        : getDelegate().getTextSize()), getDelegate().getTextSizeUnit());
                if (getDelegate().getTextBold() == TextBold.SELECT) {
                    tabTitle.setFont(isSelect ? Font.DEFAULT_BOLD : Font.DEFAULT);
                }
            }
        }
    }

    private double margin;

    private void calcIndicatorRect() {
        Component currentTabView = mTabsContainer.getComponentAt(this.mCurrentTab);
        // 滑动后你重新绘制的位置必须要加上你滑动的距离不然 就会绘制到其他位置！！
        double left = currentTabView.getLeft() + scrollOffset;
        double right = currentTabView.getRight() + scrollOffset;
        // for mIndicatorWidthEqualTitle
        if (getDelegate().getIndicatorStyle() == STYLE_NORMAL && getDelegate().isIndicatorWidthEqualTitle()) {
            Text tabTitle = FindUtils.findTextById(currentTabView, ResourceTable.Id_tv_tab_title);
            mTextPaint.setTextSize((int) getDelegate().getTextSize());
            double textWidth = mTextPaint.measureText(tabTitle.getText());
            margin = (right - left - textWidth) / 2;
        }
        if (this.targetPosition >= 0 && this.targetPosition <= mTabCount - 1) {
            Component nextTabView = mTabsContainer.getComponentAt(targetPosition);
            double nextTabLeft = nextTabView.getLeft() + scrollOffset;
            double nextTabRight = nextTabView.getRight() + scrollOffset;

            left = left + mCurrentPositionOffset * (Math.abs(nextTabLeft - left));
            right = right + mCurrentPositionOffset * (Math.abs(nextTabRight - right));
            // for mIndicatorWidthEqualTitle
            if (getDelegate().getIndicatorStyle() == STYLE_NORMAL
                    && getDelegate().isIndicatorWidthEqualTitle()) {
                Text nextTabTitle = FindUtils.findTextById(nextTabView, ResourceTable.Id_tv_tab_title);
                mTextPaint.setTextSize((int) getDelegate().getTextSize());
                double nextTextWidth = mTextPaint.measureText(nextTabTitle.getText());
                double nextMargin = (nextTabRight - nextTabLeft - nextTextWidth) / 2;
                margin = margin + mCurrentPositionOffset * (nextMargin - margin);
            }
        }

        mIndicatorRect.left = (int) left;
        mIndicatorRect.right = (int) right;
        // for mIndicatorWidthEqualTitle
        if (getDelegate().getIndicatorStyle() == STYLE_NORMAL
                || getDelegate().isIndicatorWidthEqualTitle()) {
            mIndicatorRect.left = (int) (left + margin - 1);
            mIndicatorRect.right = (int) (right - margin - 1);
        }
        mTabRect.left = (int) left;
        mTabRect.right = (int) right;
        // indicatorWidth小于0时,原jpardogo's PagerSlidingTabStrip
        if (!(getDelegate().getIndicatorWidth() < 0)) {
            // indicatorWidth大于0时,圆角矩形以及三角形
            double indicatorLeft = (currentTabView.getLeft() + (currentTabView.getWidth()
                    - (double) getDelegate().getIndicatorWidth()) / 2);

            if (this.targetPosition >= 0 && this.targetPosition <= mTabCount - 1) {
                Component nextTab = mTabsContainer.getComponentAt(targetPosition);
                indicatorLeft = indicatorLeft + mCurrentPositionOffset
                        * ((double) currentTabView.getWidth() / 2 + (double) nextTab.getWidth() / 2);
            }
            mIndicatorRect.left = (int) indicatorLeft + scrollOffset;
            mIndicatorRect.right = (mIndicatorRect.left + getDelegate().getIndicatorWidth());
        }
    }

    /**
     * setCurrentTab
     *
     * @param currentTab currentTab
     * @return SlidingTabLayout
     */
    public SlidingTabLayout setCurrentTab(int currentTab) {
        return setCurrentTab(currentTab, false);
    }

    /**
     * setCurrentTab
     *
     * @param currentTab   currentTab
     * @param smoothScroll smoothScroll
     * @return SlidingTabLayout
     */
    public SlidingTabLayout setCurrentTab(int currentTab, boolean smoothScroll) {
        size = (int) ((double) mTabsContainer.getComponentAt(currentTab).getContentPositionX()
                - (double) mTabsContainer.getComponentAt(mCurrentTab).getContentPositionX());
        if (mCurrentSelected != currentTab) {
            this.mCurrentTab = currentTab;
            this.mCurrentSelected = currentTab;
            mViewPager.setCurrentPage(currentTab, smoothScroll);
        }
        return this;
    }


    private final Paint mTextPaint = new Paint();

    private void drawDivider(Canvas canvas, int height, int paddingLeft) {
        if (getDelegate().getDividerWidth() > 0) {
            mDividerPaint.setAntiAlias(true);
            mDividerPaint.setStrokeWidth(getDelegate().getDividerWidth());
            mDividerPaint.setColor(new Color(getDelegate().getDividerColor()));
            for (int i = 0; i < mTabCount - 1; i++) {
                Component tab = mTabsContainer.getComponentAt(i);
                canvas.drawLine(paddingLeft + tab.getRight(), getDelegate().getDividerPadding(),
                        paddingLeft + tab.getRight(),
                        (float) (height - (double) getDelegate().getDividerPadding()), mDividerPaint);
            }
        }
    }

    private void drawUnderline(Canvas canvas, int height, int paddingLeft) {
        if (getDelegate().getUnderlineHeight() > 0) {
            mRectPaint.setColor(new Color(getDelegate().getUnderlineColor()));
            if (getDelegate().getUnderlineGravity() == LayoutAlignment.BOTTOM) {
                canvas.drawRect(paddingLeft, (float) (height - (double) getDelegate().getUnderlineHeight()),
                        mTabsContainer.getWidth() + paddingLeft, height, mRectPaint);
            } else {
                canvas.drawRect(paddingLeft, 0, mTabsContainer.getWidth() + paddingLeft,
                        getDelegate().getUnderlineHeight(), mRectPaint);
            }
        }
    }

    private void drawTriangle(Canvas canvas, int height, int paddingLeft) {
        if (getDelegate().getIndicatorHeight() > 0) {
            mTrianglePaint.setColor(new Color(getDelegate().getIndicatorColor()));
            mTrianglePath.reset();
            mTrianglePath.moveTo(paddingLeft + mIndicatorRect.left, height);
            mTrianglePath.lineTo((float) (paddingLeft + mIndicatorRect.left / 2
                            + mIndicatorRect.right / 2),
                    height - getDelegate().getIndicatorHeight());
            mTrianglePath.lineTo(paddingLeft + mIndicatorRect.right, height);
            mTrianglePath.close();
            canvas.drawPath(mTrianglePath, mTrianglePaint);
        }
    }

    private void drawBlock(Canvas canvas, int height, int paddingLeft) {
        if (getDelegate().getIndicatorHeight() < 0) {
            getDelegate().setIndicatorHeight(height -
                    getDelegate().getIndicatorMarginTop() -
                    getDelegate().getIndicatorMarginBottom());
        }
        if (getDelegate().getIndicatorHeight() > 0) {
            if (getDelegate().getIndicatorCornerRadius() < 0
                    || getDelegate().getIndicatorCornerRadius() >
                    (float) getDelegate().getIndicatorHeight() / 2) {
                getDelegate().setIndicatorCornerRadius((float) getDelegate().getIndicatorHeight() / 2);
            }
            mIndicatorDrawable.setRgbColor(RgbColor.fromArgbInt(getDelegate().getIndicatorColor()));
            mIndicatorDrawable.setBounds(paddingLeft + getDelegate().getIndicatorMarginLeft() +
                            mIndicatorRect.left, getDelegate().getIndicatorMarginTop(),
                    (paddingLeft + mIndicatorRect.right - getDelegate().getIndicatorMarginRight()),
                    (getDelegate().getIndicatorMarginTop() + getDelegate().getIndicatorHeight()));
            mIndicatorDrawable.setCornerRadius(getDelegate().getIndicatorCornerRadius());
            mIndicatorDrawable.drawToCanvas(canvas);
        }
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mTabCount <= 0) {
            return;
        }
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        // draw divider
        drawDivider(canvas, height, paddingLeft);
        // draw underline
        drawUnderline(canvas, height, paddingLeft);
        // draw indicator line
        calcIndicatorRect();

        if (getDelegate().getIndicatorStyle() == STYLE_TRIANGLE) {
            drawTriangle(canvas, height, paddingLeft);
        } else if (getDelegate().getIndicatorStyle() == STYLE_BLOCK) {
            drawBlock(canvas, height, paddingLeft);
        } else {
            if (getDelegate().getIndicatorHeight() > 0) {
                mIndicatorDrawable.setRgbColor(RgbColor.fromArgbInt(getDelegate().getIndicatorColor()));
                if (getDelegate().getIndicatorGravity() == LayoutAlignment.BOTTOM) {
                    mIndicatorDrawable.setBounds(paddingLeft +
                                    getDelegate().getIndicatorMarginLeft() + mIndicatorRect.left,
                            height - getDelegate().getIndicatorHeight() -
                                    getDelegate().getIndicatorMarginBottom(),
                            paddingLeft + mIndicatorRect.right -
                                    getDelegate().getIndicatorMarginRight(),
                            height - getDelegate().getIndicatorMarginBottom());
                } else {
                    mIndicatorDrawable.setBounds(paddingLeft +
                                    getDelegate().getIndicatorMarginLeft() +
                                    mIndicatorRect.left,
                            getDelegate().getIndicatorMarginTop(),
                            paddingLeft + mIndicatorRect.right - getDelegate().getIndicatorMarginRight(),
                            getDelegate().getIndicatorHeight() + getDelegate().getIndicatorMarginTop());
                }
                mIndicatorDrawable.setCornerRadius(getDelegate().getIndicatorCornerRadius());
                mIndicatorDrawable.drawToCanvas(canvas);
            }
        }
        if (isFirstCreate) {
            isFirstCreate = false;
            scrollToCurrentTab();
        }
    }

    private int targetPosition;
    private final int width = DisplayManager.getInstance().getDefaultDisplay(getContext()).get()
            .getAttributes().width;

    private void typeOne(float positionOffset, int positionOffsetPixels, boolean isToLeft) {
        float val1 = positionOffset > (float) (width - Math.abs(positionOffsetPixels)) / width ?
                (float) (width - Math.abs(positionOffsetPixels)) / width
                : (float) Math.abs(positionOffsetPixels) / width;
        float val2 = positionOffset > (float) (width - Math.abs(positionOffsetPixels)) / width ?
                -(float) (width - Math.abs(positionOffsetPixels)) / width
                : -(float) Math.abs(positionOffsetPixels) / width;
        this.mCurrentPositionOffset = isToLeft ? val1 : val2;
    }

    private void typeTwo(int positionOffsetPixels, boolean isToLeft) {
        if (Math.abs(positionOffsetPixels) > width || Math.abs(size) > width) {
            float val11 = (float) (Math.abs(positionOffsetPixels) > width ? Math.abs(positionOffsetPixels)
                    - width : width - Math.abs(positionOffsetPixels)) / width;
            float val12 = -(float) (Math.abs(positionOffsetPixels) > width ? Math.abs(positionOffsetPixels)
                    - width : width - Math.abs(positionOffsetPixels)) / width;
            this.mCurrentPositionOffset = isToLeft ? val11 : val12;
        }
    }

    private void typeThree(int position, int positionOffsetPixels, float positionOffset) {
        if ((DataUtils.getInstance().isFirstTouch() && position != 0)) {
            float val21 = (float) (Math.abs(positionOffsetPixels) > width ? Math.abs(positionOffsetPixels)
                    - width : width - Math.abs(positionOffsetPixels)) / width;
            float val22 = -(float) (Math.abs(positionOffsetPixels) > width ? Math.abs(positionOffsetPixels)
                    - width : width - Math.abs(positionOffsetPixels)) / width;
            this.mCurrentPositionOffset = positionOffset == 1.0f ? val21 : val22;
        }
    }

    private void typeElse(int position, int positionOffsetPixels, float positionOffset) {
        if (position == mTabCount - 1 && (float) (width - Math.abs(positionOffsetPixels))
                / width < positionOffset) {
            this.mCurrentPositionOffset = -(float) (width - Math.abs(positionOffsetPixels)) / width;
        }
        // 第一个条目
        if (position == 0 && (float) (width - Math.abs(positionOffsetPixels)) / width < positionOffset) {
            this.mCurrentPositionOffset = (float) (width - Math.abs(positionOffsetPixels)) / width;
        }
        // 直接选中第一个或者最后一个会出现一个超额数字
        int count = Math.abs(positionOffsetPixels) / width;
        if (positionOffset == 1.0f && Math.abs(positionOffsetPixels) > width && count > 0
                && (position == mTabCount - 1 || position == 0)) {
            this.mCurrentPositionOffset = positionOffsetPixels > 0 ?
                    -(float) (width - (Math.abs(positionOffsetPixels) - width * count)) / width
                    : (float) (width - (Math.abs(positionOffsetPixels) - width * count)) / width;
        }
    }

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
        // 返回将要滑动到的目标position;
        // 每次点击都会走 而且每次都是1.0f和屏幕宽度
        if (positionOffset == 1.0f && (positionOffsetPixels == width
                || positionOffsetPixels == -width)) {
            // 防止每次都去计算
            this.mCurrentPositionOffset = 0.0f;
            scrollToCurrentTab();
            invalidate();
            return;
        }
        boolean isToLeft = !DataUtils.getInstance().isRight();
        this.targetPosition = isToLeft ? position + 1 : position - 1;
        if (DataUtils.getInstance().isUp()) {
            // 手抬起来
            this.targetPosition = position;
            // 还原超屏大小
            size = 0;
        }
        // 不会返回滚动的方向(有个问题第一次是往右拉是0.9....开始的往左直接全是1.0  莫名其妙！！！！！)
        typeOne(positionOffset, positionOffsetPixels, isToLeft);
        // 一种情况 返回的positionOffsetPixels大于屏幕
        typeTwo(positionOffsetPixels, isToLeft);
        // 用第一次触摸的百分比来算滑动距离 解决setCurrentPosition 一直返回0.9..或者0.1f的问题
        typeThree(position, positionOffsetPixels, positionOffset);
        // 最后一个并且点击选中
        typeElse(position, positionOffsetPixels, positionOffset);

        scrollToCurrentTab();
        invalidate();
    }

    @Override
    public void onPageSlideStateChanged(int position) {
    }

    @Override
    public void onPageChosen(int position) {
        targetPosition = position;
        setCurrentTab(position);
        updateTabSelection(position);
    }

    private boolean isMove = false;

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // 用来解决pageslider 第一次强制返回1.0f问题
        int index = touchEvent.getIndex();
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                DataUtils.getInstance().setUp(false);
                downX = (double) touchEvent.getPointerScreenPosition(index).getX();
                break;
            case TouchEvent.POINT_MOVE:
                isMove = true;
                double basicX = (double) touchEvent.getPointerScreenPosition(index).getX() - downX;
                // 触摸事件只会在最后一个生效 没办法需要都联动的话只能将状态存储到一个单例类中
                DataUtils.getInstance().setRight(basicX > 0);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (isMove) {
                    // 触摸事件只会在最后一个生效 没办法需要都联动的话只能将状态存储到一个单例类中
                    DataUtils.getInstance().setUp(true);
                    DataUtils.getInstance().setFirstTouch(false);
                }
                break;
        }

        return true;
    }

}
