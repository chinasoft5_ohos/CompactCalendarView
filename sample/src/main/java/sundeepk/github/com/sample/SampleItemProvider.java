/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;

import com.github.sundeepk.compactcalendarview.FindUtils;
import ohos.agp.components.*;

import java.util.List;

/**
 * SampleItemProvider
 *
 * @author hal
 * @since 2021/06/21
 */
public class SampleItemProvider extends BaseItemProvider {
    private final List<String> mutableBookings;

    /**
     * SampleItemProvider
     *
     * @param mutableBookings mutableBookings
     */
    public SampleItemProvider(List<String> mutableBookings) {
        this.mutableBookings = mutableBookings;
    }

    @Override
    public int getCount() {
        return mutableBookings.size();
    }

    @Override
    public Object getItem(int position) {
        return mutableBookings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component view;
        ViewHolder viewHolder = null;
        if (component == null) {
            view = LayoutScatter.getInstance(componentContainer.getContext()).parse(
                    ResourceTable.Layout_item_view, componentContainer, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            view = component;
            Object tag = view.getTag();
            if (tag instanceof ViewHolder) {
                viewHolder = (ViewHolder) tag;
            }
        }
        if (viewHolder != null) {
            viewHolder.tvName.setText(mutableBookings.get(position));
        }

        return view;
    }

    /**
     * ViewHolder
     */
    public static class ViewHolder {
        /**
         * tvName
         */
        public Text tvName;

        public ViewHolder(Component component) {
            tvName = FindUtils.findTextById(component, ResourceTable.Id_tv);
        }
    }
}
