/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;

/**
 * DataUtils
 *
 * @author hal
 * @since 2021/06/21
 */
public class DataUtils {
    private static volatile DataUtils dataUtils;
    /**
     * 手是否抬起
     */
    private boolean up;
    /**
     * 是否第一次触摸
     */
    private boolean isFirstTouch;

    /**
     * 滑动方向
     */
    private boolean isRight;

    /**
     * 单例
     *
     * @return DataUtils
     */
    public static DataUtils getInstance() {
        if (dataUtils == null) {
            synchronized (DataUtils.class) {
                dataUtils = new DataUtils();
            }
        }
        return dataUtils;
    }

    /**
     * 手指是否抬起
     *
     * @return 抬起
     */
    public boolean isUp() {
        return up;
    }

    /**
     * 设置手指状态
     *
     * @param up 是否抬起
     */
    public void setUp(boolean up) {
        this.up = up;
    }

    /**
     * isFirstTouch
     *
     * @return boolean
     */
    public boolean isFirstTouch() {
        return isFirstTouch;
    }

    /**
     * 设置触摸状态
     *
     * @param firstTouch 是不是第一次触摸
     */
    public void setFirstTouch(boolean firstTouch) {
        isFirstTouch = firstTouch;
    }

    /**
     * 是否向右滑动
     *
     * @return 右边
     */
    public boolean isRight() {
        return isRight;
    }

    /**
     * 设置滑动方向
     *
     * @param right 滑动方向
     */
    public void setRight(boolean right) {
        isRight = right;
    }
}
