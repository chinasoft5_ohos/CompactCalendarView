/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;


import com.github.sundeepk.compactcalendarview.AttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;

/**
 * TabCommonSlidingDelegate
 *
 * @Author: AriesHoo on 2018/11/30 16:59
 * @E-Mail: AriesHoo@126.com
 * @since 2021/06/21
 */
public class TabCommonSlidingDelegate extends TabLayoutDelegate {
    /**
     * mIndicatorStyle
     */
    protected int mIndicatorStyle;
    /**
     * mIndicatorWidth
     */
    protected int mIndicatorWidth;
    /**
     * mIndicatorGravity
     */
    protected int mIndicatorGravity;
    /**
     * underline
     */
    protected int mUnderlineColor;
    /**
     * mUnderlineHeight
     */
    protected int mUnderlineHeight;
    /**
     * mUnderlineGravity
     */
    protected int mUnderlineGravity;


    public TabCommonSlidingDelegate(Component view, AttrSet attrs, ITabLayout iTabLayout) {
        super(view, attrs, iTabLayout);
        mIndicatorStyle = getType(AttrUtils.getStringFromAttr(attrs, "tl_indicator_style", "NORMAL"));
        mIndicatorColor = AttrUtils.getColorFromAttr(attrs, "tl_indicator_color",
                Color.getIntColor(mIndicatorStyle == IndicatorStyle.BLOCK ? "#4B6A87" : "#ffffff"));
        int temp = mIndicatorStyle == IndicatorStyle.BLOCK ? -1 : 2;
        mIndicatorHeight = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_height",
                dp2px(mIndicatorStyle == IndicatorStyle.TRIANGLE ? 4 : temp));
        mIndicatorWidth = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_width",
                dp2px(mIndicatorStyle == IndicatorStyle.TRIANGLE ? 10 : -1));
        mIndicatorMarginTop = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_top",
                dp2px(mIndicatorStyle == IndicatorStyle.BLOCK ? 7 : 0));
        mIndicatorMarginBottom = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_bottom",
                dp2px(mIndicatorStyle == IndicatorStyle.BLOCK ? 7 : 0));
        mIndicatorCornerRadius = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_corner_radius",
                dp2px(mIndicatorStyle == IndicatorStyle.BLOCK ? -1 : 0));
        mIndicatorGravity = AttrUtils.getIntegerFromAttr(attrs, "tl_indicator_gravity",
                LayoutAlignment.BOTTOM);

        mUnderlineColor = AttrUtils.getColorFromAttr(attrs, "tl_underline_color",
                Color.getIntColor("#ffffff"));
        mUnderlineHeight = AttrUtils.getDimensionFromAttr(attrs, "tl_underline_height", dp2px(0));
        mUnderlineGravity = AttrUtils.getIntegerFromAttr(attrs, "tl_underline_gravity",
                LayoutAlignment.BOTTOM);
    }

    private int getType(String name) {
        int type = IndicatorStyle.NORMAL;
        switch (name) {
            case "NORMAL":
                type = IndicatorStyle.NORMAL;
                break;
            case "TRIANGLE":
                type = IndicatorStyle.TRIANGLE;
                break;
            case "BLOCK":
                type = IndicatorStyle.BLOCK;
                break;
        }
        return type;
    }

    /**
     * getIndicatorStyle
     *
     * @return mIndicatorStyle
     */
    public int getIndicatorStyle() {
        return mIndicatorStyle;
    }

    /**
     * getIndicatorWidth
     *
     * @return mIndicatorWidth
     */
    public int getIndicatorWidth() {
        return mIndicatorWidth;
    }

    /**
     * getIndicatorGravity
     *
     * @return mIndicatorGravity
     */
    public int getIndicatorGravity() {
        return mIndicatorGravity;
    }

    /**
     * getUnderlineColor
     *
     * @return mUnderlineColor
     */
    public int getUnderlineColor() {
        return mUnderlineColor;
    }

    /**
     * getUnderlineHeight
     *
     * @return mUnderlineHeight
     */
    public float getUnderlineHeight() {
        return mUnderlineHeight;
    }

    /**
     * getUnderlineGravity
     *
     * @return mUnderlineGravity
     */
    public int getUnderlineGravity() {
        return mUnderlineGravity;
    }
}
