package sundeepk.github.com.sample;


import com.github.sundeepk.compactcalendarview.LogUtil;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.agp.components.Text;

import java.util.HashMap;
import java.util.Map;

/**
 * ViewPagerAdapter
 *
 * @author hal
 * @since 2021/06/21
 */
public class ViewPagerAdapter extends PageSliderProvider {
    private final Map<Integer, Component> cacheMap = new HashMap<>();

    private final Text tvTitle;

    private String[] titles;

    private final int numbOfTabs;

    public ViewPagerAdapter(String[] titles, int mNumbOfTabs, Text tvTitle) {
        if (titles != null) {
            this.titles = titles.clone();
        }
        this.numbOfTabs = mNumbOfTabs;
        this.tvTitle = tvTitle;
    }

    @Override
    public String getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return numbOfTabs;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        if (position == 0) {
            if (cacheMap.get(position) == null) {
                CompactCalendarTab compactCalendarTab = new CompactCalendarTab(componentContainer.getContext());
                compactCalendarTab.setTitle(tvTitle);
                componentContainer.addComponent(compactCalendarTab);
                return componentContainer;
            }
        } else {
            if (cacheMap.get(position) == null) {
                Tab2 tab2 = new Tab2(componentContainer.getContext());
                componentContainer.addComponent(tab2);
                return componentContainer;
            }
        }
        for (int j = 0; j < componentContainer.getChildCount(); j++) {
            if (componentContainer.getComponentAt(j) instanceof CompactCalendarTab && position == 0) {
                componentContainer.removeComponentAt(j);
                break;
            } else if (componentContainer.getComponentAt(j) instanceof Tab2 && position == 1) {
                componentContainer.removeComponentAt(j);
                break;
            } else {
                LogUtil.loge("ViewPagerAdapter");
            }
        }
        if (position == 0 && cacheMap.get(position) instanceof CompactCalendarTab) {
            if (cacheMap.get(position) instanceof CompactCalendarTab) {
                CompactCalendarTab compactCalendarTab = (CompactCalendarTab) cacheMap.get(position);
                compactCalendarTab.setViewRefresh();
            }
        }

        componentContainer.addComponent(cacheMap.get(position));
        return componentContainer;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object object) {
        if (object instanceof Component) {
            componentContainer.removeComponent((Component) object);
        }
        if (object instanceof ComponentContainer) {
            cacheMap.put(position, ((ComponentContainer) object).getComponentAt(0));
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}