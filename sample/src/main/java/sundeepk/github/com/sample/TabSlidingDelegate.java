/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;


import com.github.sundeepk.compactcalendarview.AttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;

/**
 * TabSlidingDelegate
 *
 * @Author: AriesHoo on 2018/12/3 9:19
 * @E-Mail: AriesHoo@126.com
 * @Description: TabSlidingDelegate
 * @since 2021/06/21
 */
public class TabSlidingDelegate extends TabCommonSlidingDelegate {
    private final boolean mIndicatorWidthEqualTitle;

    public TabSlidingDelegate(Component view, AttrSet attrs, ITabLayout iTabLayout) {
        super(view, attrs, iTabLayout);
        mTabSpaceEqual = AttrUtils.getBooleanFromAttr(attrs, "tl_tab_space_equal", false);
        mTabPadding = AttrUtils.getDimensionFromAttr(attrs, "tl_tab_padding",
                mTabSpaceEqual || mTabWidth > 0 ? dp2px(0) : dp2px(20));
        mIndicatorWidthEqualTitle = AttrUtils.getBooleanFromAttr(attrs,
                "tl_indicator_width_equal_title", false);
    }

    /**
     * mIndicatorWidthEqualTitle
     *
     * @return mIndicatorWidthEqualTitle
     */
    public boolean isIndicatorWidthEqualTitle() {
        return mIndicatorWidthEqualTitle;
    }
}
