/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;

import com.github.sundeepk.compactcalendarview.AttrUtils;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Context;

import static ohos.agp.components.Text.TextSizeType.PX;

/**
 * TabLayoutDelegate
 *
 * @Author: AriesHoo on 2018/11/30 15:55
 * @E-Mail: AriesHoo@126.com
 * @Function: TabLayout属性代理类
 * @Description: 1、2018-12-13 09:40:01 新增选中文字字号设置 textSelectSize
 * @since 2021/06/21
 */
public class TabLayoutDelegate {
    /**
     * mITabLayout
     */
    protected ITabLayout mITabLayout;
    /**
     * mView
     */
    protected Component mView;
    /**
     * mContext
     */
    protected Context mContext;


    /**
     * mIndicatorColor
     */
    protected int mIndicatorColor;
    /**
     * mIndicatorHeight
     */
    protected int mIndicatorHeight;
    /**
     * mIndicatorCornerRadius
     */
    protected float mIndicatorCornerRadius;
    /**
     * mIndicatorMarginLeft
     */
    protected int mIndicatorMarginLeft;
    /**
     * mIndicatorMarginTop
     */
    protected int mIndicatorMarginTop;
    /**
     * mIndicatorMarginRight
     */
    protected int mIndicatorMarginRight;
    /**
     * mIndicatorMarginBottom
     */
    protected int mIndicatorMarginBottom;

    /**
     * mTabPadding
     */
    protected int mTabPadding;
    /**
     * mTabSpaceEqual
     */
    protected boolean mTabSpaceEqual;
    /**
     * mTabWidth
     */
    protected int mTabWidth;

    /**
     * divider
     */
    protected int mDividerColor;
    /**
     * mDividerWidth
     */
    protected float mDividerWidth;
    /**
     * mDividerPadding
     */
    protected float mDividerPadding;

    /**
     * title
     */
    protected Text.TextSizeType mTextSizeUnit = PX;
    /**
     * mTextSize
     */
    protected float mTextSize;
    /**
     * mTextSelectSize
     */
    protected float mTextSelectSize;
    /**
     * mTextSelectColor
     */
    protected int mTextSelectColor;
    /**
     * mTextUnSelectColor
     */
    protected int mTextUnSelectColor;
    /**
     * mTextBold
     */
    protected int mTextBold;
    /**
     * mTextAllCaps
     */
    protected boolean mTextAllCaps;


    public TabLayoutDelegate(Component view, AttrSet attrs, ITabLayout iTabLayout) {
        mITabLayout = iTabLayout;
        mView = view;
        mContext = view.getContext();
        mIndicatorColor = AttrUtils.getColorFromAttr(attrs, "tl_indicator_color", Color.getIntColor("#4B6A87"));
        mIndicatorHeight = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_height", dp2px(4));
        mIndicatorCornerRadius = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_corner_radius", dp2px(0));
        mIndicatorMarginLeft = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_left", dp2px(0));
        mIndicatorMarginTop = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_top", dp2px(0));
        mIndicatorMarginRight = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_right", dp2px(0));
        mIndicatorMarginBottom = AttrUtils.getDimensionFromAttr(attrs, "tl_indicator_margin_bottom", dp2px(0));

        mDividerColor = AttrUtils.getColorFromAttr(attrs, "tl_divider_color", Color.getIntColor("#ffffff"));
        mDividerWidth = AttrUtils.getDimensionFromAttr(attrs, "tl_divider_width", dp2px(0));
        mDividerPadding = AttrUtils.getDimensionFromAttr(attrs, "tl_divider_padding", dp2px(12));

        mTextSize = AttrUtils.getDimensionFromAttr(attrs, "tl_textSize", dp2px(14));
        mTextSelectSize = AttrUtils.getDimensionFromAttr(attrs, "tl_textSelectSize", 0);
        mTextSelectColor = AttrUtils.getColorFromAttr(attrs, "tl_textSelectColor", Color.getIntColor("#ffffff"));
        mTextUnSelectColor = AttrUtils.getColorFromAttr(attrs, "tl_textUnSelectColor", Color.getIntColor("#AAffffff"));
        mTextBold = AttrUtils.getIntegerFromAttr(attrs, "tl_textBold", TextBold.NONE);
        mTextAllCaps = AttrUtils.getBooleanFromAttr(attrs, "tl_textAllCaps", false);

        mTabSpaceEqual = AttrUtils.getBooleanFromAttr(attrs, "tl_tab_space_equal", true);
        mTabWidth = AttrUtils.getDimensionFromAttr(attrs, "tl_tab_width", dp2px(-1));
        mTabPadding = AttrUtils.getDimensionFromAttr(attrs, "tl_tab_padding",
                mTabSpaceEqual || mTabWidth > 0 ?
                        dp2px(0) : dp2px(10));

        mTextSelectSize = mTextSelectSize == 0 ? mTextSize : mTextSelectSize;
    }

    /**
     * back
     */
    protected void back() {
        mView.invalidate();
    }


    /**
     * 新增方法
     *
     * @param indicatorHeight 高度
     */
    public void setIndicatorHeight(int indicatorHeight) {
        this.mIndicatorHeight = indicatorHeight;
        back();
    }

    /**
     * setIndicatorCornerRadius
     *
     * @param indicatorCornerRadius indicatorCornerRadius
     */
    public void setIndicatorCornerRadius(float indicatorCornerRadius) {
        this.mIndicatorCornerRadius = dp2px(indicatorCornerRadius);
        back();
    }


    /**
     * mIndicatorColor
     *
     * @return mIndicatorColor
     */
    public int getIndicatorColor() {
        return mIndicatorColor;
    }

    /**
     * mIndicatorHeight
     *
     * @return mIndicatorHeight
     */
    public int getIndicatorHeight() {
        return mIndicatorHeight;
    }


    /**
     * mIndicatorCornerRadius
     *
     * @return mIndicatorCornerRadius
     */
    public float getIndicatorCornerRadius() {
        return mIndicatorCornerRadius;
    }

    /**
     * mIndicatorCornerRadius
     *
     * @return mIndicatorCornerRadius
     */
    public int getIndicatorMarginLeft() {
        return mIndicatorMarginLeft;
    }

    /**
     * mIndicatorMarginTop
     *
     * @return mIndicatorMarginTop
     */
    public int getIndicatorMarginTop() {
        return mIndicatorMarginTop;
    }

    /**
     * mIndicatorMarginRight
     *
     * @return mIndicatorMarginRight
     */
    public int getIndicatorMarginRight() {
        return mIndicatorMarginRight;
    }

    /**
     * mIndicatorMarginBottom
     *
     * @return mIndicatorMarginBottom
     */
    public int getIndicatorMarginBottom() {
        return mIndicatorMarginBottom;
    }


    /**
     * mTabPadding
     *
     * @return mTabPadding
     */
    public int getTabPadding() {
        return mTabPadding;
    }

    /**
     * mTabSpaceEqual
     *
     * @return mTabSpaceEqual
     */
    public boolean isTabSpaceEqual() {
        return mTabSpaceEqual;
    }

    /**
     * mTabWidth
     *
     * @return mTabWidth
     */
    public int getTabWidth() {
        return mTabWidth;
    }

    /**
     * mDividerColor
     *
     * @return mDividerColor
     */
    public int getDividerColor() {
        return mDividerColor;
    }

    /**
     * mDividerWidth
     *
     * @return mDividerWidth
     */
    public float getDividerWidth() {
        return mDividerWidth;
    }

    /**
     * mDividerPadding
     *
     * @return mDividerPadding
     */
    public float getDividerPadding() {
        return mDividerPadding;
    }

    /**
     * mTextSizeUnit
     *
     * @return mTextSizeUnit
     */
    public Text.TextSizeType getTextSizeUnit() {
        return mTextSizeUnit;
    }

    /**
     * mTextSelectSize
     *
     * @return mTextSelectSize
     */
    public float getTextSelectSize() {
        return mTextSelectSize;
    }

    /**
     * mTextSize
     *
     * @return mTextSize
     */
    public float getTextSize() {
        return mTextSize;
    }

    /**
     * mTextSelectColor
     *
     * @return mTextSelectColor
     */
    public int getTextSelectColor() {
        return mTextSelectColor;
    }

    /**
     * mTextUnSelectColor
     *
     * @return mTextUnSelectColor
     */
    public int getTextUnSelectColor() {
        return mTextUnSelectColor;
    }

    /**
     * mTextBold
     *
     * @return mTextBold
     */
    public int getTextBold() {
        return mTextBold;
    }

    /**
     * mTextAllCaps
     *
     * @return mTextAllCaps
     */
    public boolean isTextAllCaps() {
        return mTextAllCaps;
    }

    /**
     * dp2px
     *
     * @param dp dp
     * @return int
     */
    public int dp2px(float dp) {
        return AttrHelper.vp2px(dp, mContext);
    }
}
