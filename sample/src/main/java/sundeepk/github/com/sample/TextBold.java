/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */

package sundeepk.github.com.sample;

/**
 * TextBold
 *
 * @Author: AriesHoo on 2018/11/30 11:28
 * @E-Mail: AriesHoo@126.com
 * @Function: 文字粗体属性
 * @Description: TextBold
 * @since 2021/06/21
 */
public class TextBold {
    /**
     * 不设置文字粗体效果
     */
    public static final int NONE = 0;
    /**
     * 选中文字粗体效果
     */
    public static final int SELECT = 1;
    /**
     * 文字始终设置粗体效果
     */
    public static final int BOTH = 2;
}
