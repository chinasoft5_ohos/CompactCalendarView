package sundeepk.github.com.sample;


import com.github.sundeepk.compactcalendarview.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * Tab2
 *
 * @author hal
 * @since 2021/06/21
 */
public class Tab2 extends DependentLayout {
    public Tab2(Context context) {
        this(context, null);
    }

    public Tab2(Context context, AttrSet attrSet) {
        super(context, attrSet);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_tab_2, this, false);
        addComponent(component);
    }
}
