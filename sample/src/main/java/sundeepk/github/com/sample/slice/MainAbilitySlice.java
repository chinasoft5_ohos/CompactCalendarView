package sundeepk.github.com.sample.slice;

import com.github.sundeepk.compactcalendarview.FindUtils;
import ohos.agp.components.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import sundeepk.github.com.sample.*;

/**
 * MainAbilitySlice
 *
 * @author hal
 * @since 2021/06/21
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    /**
     * 标题列表。
     */
    private final String[] titles = {"HOME", "EVENTS"};
    private MenuDialog menuDialog;

    @Override
    public void onStart(Intent intent) {
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#767676"));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text title = FindUtils.findTextById(getAbility(), ResourceTable.Id_tv_title);
        Image img = FindUtils.findImageById(getAbility(), ResourceTable.Id_img);

        // Creating The ViewPagerAdapter and Passing Fragment Manager,
        // Titles fot the Tabs and Number Of Tabs.
        int numberOfTabs = 2;
        ViewPagerAdapter adapter = new ViewPagerAdapter(titles, numberOfTabs, title);

        // Assigning ViewPager View and setting the adapter
        PageSlider pager = FindUtils.findPageById(getAbility(), ResourceTable.Id_pager);
        pager.setProvider(adapter);

        // Assiging the Sliding Tab Layout View
        Component component = findComponentById(ResourceTable.Id_tl_1);
        SlidingTabLayout tabLayout1 = null;
        if (component instanceof SlidingTabLayout) {
            tabLayout1 = (SlidingTabLayout) component;
        }
        tabLayout1.setViewPager(pager);
        img.setClickedListener(component1 -> {
            int screenWidth = AttrHelper.vp2px(180, getContext());
            int menuHeight = AttrHelper.vp2px(50, getContext());
            menuDialog = new MenuDialog(getContext(), img, screenWidth, menuHeight, ResourceTable.Layout_menu_tab);
            menuDialog.initListener(MainAbilitySlice.this);
            menuDialog.show();
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        menuDialog.destroy();
    }
}
