package com.github.sundeepk.compactcalendarview.domain;


/**
 * Event
 *
 * @since 2021/06/21
 */
public class Event {
    private final int color;
    private final long timeInMillis;
    private final Object data;

    /**
     * 构造函数
     *
     * @param color        color
     * @param timeInMillis color
     */
    public Event(int color, long timeInMillis) {
        this(color, timeInMillis, null);
    }

    /**
     * 构造函数
     *
     * @param color        color
     * @param timeInMillis timeInMillis
     * @param data         data
     */
    public Event(int color, long timeInMillis, Object data) {
        this.color = color;
        this.timeInMillis = timeInMillis;
        this.data = data;
    }

    public int getColor() {
        return color;
    }

    public long getTimeInMillis() {
        return timeInMillis;
    }

    public Object getData() {
        return data;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Event event = (Event) object;

        if (color != event.color) {
            return false;
        }
        if (timeInMillis != event.timeInMillis) {
            return false;
        }
        return data != null ? data.equals(event.data) : event.data == null;
    }

    @Override
    public int hashCode() {
        int result = color;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "color=" + color +
                ", timeInMillis=" + timeInMillis +
                ", data=" + data +
                '}';
    }
}
