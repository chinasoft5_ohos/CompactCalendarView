/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */


package com.github.sundeepk.compactcalendarview;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;

import java.util.Optional;

/**
 * 代码检查没办法！！！
 *
 * @author hal
 * @since 2021/06/21
 */
public class FindUtils {
    /**
     * text
     *
     * @param component component
     * @param id        id
     * @return Text
     */
    public static Text findTextById(Component component, int id) {
        Component componentById = component.findComponentById(id);
        if (componentById instanceof Text) {
            return (Text) componentById;
        }
        return new Text(component.getContext());
    }

    /**
     * text
     *
     * @param ability ability
     * @param id      id
     * @return Text
     */
    public static Text findTextById(Ability ability, int id) {
        Component componentById = ability.findComponentById(id);
        if (componentById instanceof Text) {
            return (Text) componentById;
        }
        return new Text(ability);
    }

    /**
     * Image
     *
     * @param component component
     * @param id        id
     * @return Image
     */
    public static Image findImageById(Component component, int id) {
        Component componentById = component.findComponentById(id);
        if (componentById instanceof Image) {
            return (Image) componentById;
        }
        return new Image(component.getContext());
    }

    /**
     * Image
     *
     * @param ability ability
     * @param id      id
     * @return Image
     */
    public static Image findImageById(Ability ability, int id) {
        Component componentById = ability.findComponentById(id);
        if (componentById instanceof Image) {
            return (Image) componentById;
        }
        return new Image(ability);
    }

    /**
     * PageSlider
     *
     * @param ability ability
     * @param id      id
     * @return PageSlider
     */
    public static PageSlider findPageById(Ability ability, int id) {
        Component componentById = ability.findComponentById(id);
        if (componentById instanceof PageSlider) {
            return (PageSlider) componentById;
        }
        return new PageSlider(ability);
    }

}
