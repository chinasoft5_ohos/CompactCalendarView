package com.github.sundeepk.compactcalendarview.comparators;

import com.github.sundeepk.compactcalendarview.domain.Event;

import java.io.Serializable;
import java.util.Comparator;

/**
 * EventComparator
 *
 * @since 2021/06/21
 */
public class EventComparator implements Comparator<Event>, Serializable {
    @Override
    public int compare(Event lhs, Event rhs) {
        return Long.compare(lhs.getTimeInMillis(), rhs.getTimeInMillis());
    }
}
