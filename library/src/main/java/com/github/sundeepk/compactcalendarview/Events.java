package com.github.sundeepk.compactcalendarview;

import com.github.sundeepk.compactcalendarview.domain.Event;

import java.util.List;
import java.util.Objects;

class Events {
    private final List<Event> events;
    private final long timeInMillis;

    Events(long timeInMillis, List<Event> events) {
        this.timeInMillis = timeInMillis;
        this.events = events;
    }

    long getTimeInMillis() {
        return timeInMillis;
    }

    List<Event> getEvents() {
        return events;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Events event = (Events) object;

        if (timeInMillis != event.timeInMillis) {
            return false;
        }
        return Objects.equals(events, event.events);
    }

    @Override
    public int hashCode() {
        int result = events != null ? events.hashCode() : 0;
        result = 31 * result + (int) (timeInMillis ^ (timeInMillis >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Events{" +
                "events=" + events +
                ", timeInMillis=" + timeInMillis +
                '}';
    }
}
