package com.github.sundeepk.compactcalendarview;


import com.github.sundeepk.compactcalendarview.domain.Event;
import com.github.sundeepk.compactcalendarview.lib.Scroller;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.VelocityDetector;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.*;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.github.sundeepk.compactcalendarview.CompactCalendarView.CompactCalendarViewListener;
import static com.github.sundeepk.compactcalendarview.CompactCalendarView.FILL_LARGE_INDICATOR;
import static com.github.sundeepk.compactcalendarview.CompactCalendarView.NO_FILL_LARGE_INDICATOR;
import static com.github.sundeepk.compactcalendarview.CompactCalendarView.SMALL_INDICATOR;


class CompactCalendarController {
    /**
     * IDLE
     */
    public static final int IDLE = 0;

    /**
     * EXPOSE_CALENDAR_ANIMATION
     */
    public static final int EXPOSE_CALENDAR_ANIMATION = 1;

    /**
     * EXPAND_COLLAPSE_CALENDAR
     */
    public static final int EXPAND_COLLAPSE_CALENDAR = 2;

    /**
     * ANIMATE_INDICATORS
     */
    public static final int ANIMATE_INDICATORS = 3;

    private static final int VELOCITY_UNIT_PIXELS_PER_SECOND = 1000;

    private static final int LAST_FLING_THRESHOLD_MILLIS = 300;

    private static final int DAYS_IN_WEEK = 7;

    private static final float SNAP_VELOCITY_DIP_PER_SECOND = 400;

    private final Scroller scroller;

    private int eventIndicatorStyle = SMALL_INDICATOR;
    private int currentDayIndicatorStyle = FILL_LARGE_INDICATOR;
    private int currentSelectedDayIndicatorStyle = SMALL_INDICATOR;
    private int paddingWidth = 40;
    private int paddingHeight = 40;
    private int textHeight;
    private int widthPerDay;
    private int monthsScrolledSoFar;
    private int heightPerDay;
    private int textSize = 30;
    private int width;
    private int height;
    private int paddingRight;
    private int paddingLeft;
    private int densityAdjustedSnapVelocity;
    private int distanceThresholdForAutoScroll;
    private int targetHeight;
    private int animationStatus = 0;
    private int firstDayOfWeekToDraw = Calendar.MONDAY;
    private float xIndicatorOffset;
    private float bigCircleIndicatorRadius;
    private float smallIndicatorRadius;
    private float growFactor = 0f;
    private float screenDensity = 1;
    private float growfactorIndicator;
    private float distanceX;
    private long lastAutoScrollFromFling;

    private boolean useThreeLetterAbbreviation = false;
    private boolean isSmoothScrolling;
    private boolean isScrolling;
    private boolean shouldDrawDaysHeader = true;
    private boolean shouldDrawIndicatorsBelowSelectedDays = false;
    private boolean displayOtherMonthDays;
    private boolean shouldSelectFirstDayOfMonthOnScroll = true;
    private boolean isRtl = false;

    private CompactCalendarViewListener listener;
    private VelocityDetector velocityTracker;
    private Direction currentDirection = Direction.NONE;
    private Date currentDate = new Date();
    private Locale locale;
    private Calendar currentCalender;
    private Calendar todayCalender;
    private Calendar calendarWithFirstDayOfMonth;
    private Calendar eventsCalendar;
    private EventsContainer eventsContainer;
    private final Point accumulatedScrollOffset = new Point();
    private final Paint dayPaint;
    private final Paint background = new Paint();
    private final Rect textSizeRect;
    private String[] dayColumnNames;

    // colors
    private int multiEventIndicatorColor;
    private int currentDayBackgroundColor;
    private int currentDayTextColor;
    private int calenderTextColor;
    private int currentSelectedDayBackgroundColor;
    private int currentSelectedDayTextColor;
    private int calenderBackgroundColor = Color.WHITE.getValue();
    private int otherMonthDaysTextColor;
    private TimeZone timeZone;

    /**
     * Only used in onDrawCurrentMonth to temporarily calculate previous month days
     */
    private Calendar tempPreviousMonthCalendar;

    private enum Direction {
        NONE, HORIZONTAL, VERTICAL
    }

    CompactCalendarController(Paint dayPaint, Scroller scroller, Rect textSizeRect, AttrSet attrs,
                              Context context, int currentDayBackgroundColor, int calenderTextColor,
                              int currentSelectedDayBackgroundColor, VelocityDetector velocityTracker,
                              int multiEventIndicatorColor, EventsContainer eventsContainer,
                              Locale locale, TimeZone timeZone) {
        this.scroller = scroller;
        this.dayPaint = dayPaint;
        this.textSizeRect = textSizeRect;
        this.currentDayBackgroundColor = currentDayBackgroundColor;
        this.calenderTextColor = calenderTextColor;
        this.currentSelectedDayBackgroundColor = currentSelectedDayBackgroundColor;
        this.otherMonthDaysTextColor = calenderTextColor;
        this.velocityTracker = velocityTracker;
        this.multiEventIndicatorColor = multiEventIndicatorColor;
        this.eventsContainer = eventsContainer;
        this.locale = locale;
        this.timeZone = timeZone;
        this.displayOtherMonthDays = false;
        loadAttributes(attrs, context);
        init(context);
    }

    private void loadAttributes(AttrSet attrs, Context context) {
        if (attrs != null && context != null) {
            currentDayBackgroundColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarCurrentDayBackgroundColor", currentDayBackgroundColor);
            calenderTextColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarTextColor", calenderTextColor);
            currentDayTextColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarCurrentDayTextColor", calenderTextColor);
            otherMonthDaysTextColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarOtherMonthDaysTextColor", otherMonthDaysTextColor);
            currentSelectedDayBackgroundColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarCurrentSelectedDayBackgroundColor",
                    currentSelectedDayBackgroundColor);
            currentSelectedDayTextColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarCurrentSelectedDayTextColor", calenderTextColor);
            calenderBackgroundColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarBackgroundColor", calenderBackgroundColor);
            multiEventIndicatorColor = AttrUtils.getColorFromAttr(attrs,
                    "compactCalendarMultiEventIndicatorColor", multiEventIndicatorColor);
            textSize = AttrUtils.getDimensionFromAttr(attrs, "compactCalendarTextSize",
                    AttrHelper.fp2px(textSize, context));
            targetHeight = AttrUtils.getDimensionFromAttr(attrs, "compactCalendarTargetHeight",
                    AttrHelper.vp2px(targetHeight, context));
            eventIndicatorStyle = getEventIndicatorStyle(AttrUtils.getStringFromAttr(attrs,
                    "compactCalendarEventIndicatorStyle", "small_indicator"));
            currentDayIndicatorStyle = getEventIndicatorStyle(AttrUtils.getStringFromAttr(attrs,
                    "compactCalendarCurrentDayIndicatorStyle", "fill_large_indicator"));
            currentSelectedDayIndicatorStyle = getEventIndicatorStyle(AttrUtils.getStringFromAttr(attrs,
                    "compactCalendarCurrentSelectedDayIndicatorStyle",
                    "fill_large_indicator"));
            displayOtherMonthDays = AttrUtils.getBooleanFromAttr(attrs,
                    "compactCalendarDisplayOtherMonthDays", displayOtherMonthDays);
            shouldSelectFirstDayOfMonthOnScroll = AttrUtils.getBooleanFromAttr(attrs,
                    "compactCalendarShouldSelectFirstDayOfMonthOnScroll",
                    shouldSelectFirstDayOfMonthOnScroll);
        }
    }

    private int getEventIndicatorStyle(String style) {
        switch (style) {
            case "fill_large_indicator":
                return FILL_LARGE_INDICATOR;
            case "no_fill_large_indicator":
                return NO_FILL_LARGE_INDICATOR;
            case "small_indicator":
                return SMALL_INDICATOR;
        }
        return SMALL_INDICATOR;
    }


    private void init(Context context) {
        currentCalender = Calendar.getInstance(timeZone, locale);
        todayCalender = Calendar.getInstance(timeZone, locale);
        calendarWithFirstDayOfMonth = Calendar.getInstance(timeZone, locale);
        eventsCalendar = Calendar.getInstance(timeZone, locale);
        tempPreviousMonthCalendar = Calendar.getInstance(timeZone, locale);

        eventsCalendar.setMinimalDaysInFirstWeek(1);
        calendarWithFirstDayOfMonth.setMinimalDaysInFirstWeek(1);
        todayCalender.setMinimalDaysInFirstWeek(1);
        currentCalender.setMinimalDaysInFirstWeek(1);
        tempPreviousMonthCalendar.setMinimalDaysInFirstWeek(1);

        setFirstDayOfWeek(firstDayOfWeekToDraw);

        setUseWeekDayAbbreviation(false);
        dayPaint.setTextAlign(TextAlignment.CENTER);
        dayPaint.setStyle(Paint.Style.STROKE_STYLE);
        dayPaint.setAntiAlias(true);
        dayPaint.setFont(Font.SANS_SERIF);
        dayPaint.setTextSize(textSize);
        dayPaint.setColor(new Color(calenderTextColor));
        dayPaint.getTextBounds("31");
        textHeight = textSizeRect.getHeight() * 3;

        todayCalender.setTime(new Date());
        setToMidnight(todayCalender);

        currentCalender.setTime(currentDate);
        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentDate, -monthsScrolledSoFar, 0);

        initScreenDensityRelatedValues(context);

        xIndicatorOffset = 3.5f * screenDensity;

        // scale small indicator by screen density
        smallIndicatorRadius = 2.5f * screenDensity;

        // just set a default growFactor to draw full calendar when initialised
        growFactor = Integer.MAX_VALUE;
    }

    private void initScreenDensityRelatedValues(Context context) {
        if (context != null) {
            screenDensity = DisplayManager.getInstance().getDefaultDisplay(
                    context).get().getAttributes().densityPixels;

            densityAdjustedSnapVelocity = (int) (screenDensity * SNAP_VELOCITY_DIP_PER_SECOND);
        }
    }

    private void setCalenderToFirstDayOfMonth(Calendar calendarWithFirstDayOfMonth, Date currentDate,
                                              int scrollOffset, int monthOffset) {
        setMonthOffset(calendarWithFirstDayOfMonth, currentDate, scrollOffset, monthOffset);
        calendarWithFirstDayOfMonth.set(Calendar.DAY_OF_MONTH, 1);
    }

    private void setMonthOffset(Calendar calendarWithFirstDayOfMonth,
                                Date currentDate, int scrollOffset, int monthOffset) {
        calendarWithFirstDayOfMonth.setTime(currentDate);
        calendarWithFirstDayOfMonth.add(Calendar.MONTH, scrollOffset + monthOffset);
        calendarWithFirstDayOfMonth.set(Calendar.HOUR_OF_DAY, 0);
        calendarWithFirstDayOfMonth.set(Calendar.MINUTE, 0);
        calendarWithFirstDayOfMonth.set(Calendar.SECOND, 0);
        calendarWithFirstDayOfMonth.set(Calendar.MILLISECOND, 0);
    }

    void setIsRtl(boolean isRtl) {
        this.isRtl = isRtl;
    }

    void setShouldSelectFirstDayOfMonthOnScroll(boolean shouldSelectFirstDayOfMonthOnScroll) {
        this.shouldSelectFirstDayOfMonthOnScroll = shouldSelectFirstDayOfMonthOnScroll;
    }

    void setDisplayOtherMonthDays(boolean displayOtherMonthDays) {
        this.displayOtherMonthDays = displayOtherMonthDays;
    }

    void shouldDrawIndicatorsBelowSelectedDays(boolean shouldDrawIndicatorsBelowSelectedDays) {
        this.shouldDrawIndicatorsBelowSelectedDays = shouldDrawIndicatorsBelowSelectedDays;
    }

    void setCurrentDayIndicatorStyle(int currentDayIndicatorStyle) {
        this.currentDayIndicatorStyle = currentDayIndicatorStyle;
    }

    void setEventIndicatorStyle(int eventIndicatorStyle) {
        this.eventIndicatorStyle = eventIndicatorStyle;
    }

    void setCurrentSelectedDayIndicatorStyle(int currentSelectedDayIndicatorStyle) {
        this.currentSelectedDayIndicatorStyle = currentSelectedDayIndicatorStyle;
    }

    void setTargetHeight(int targetHeight) {
        this.targetHeight = targetHeight;
    }

    float getScreenDensity() {
        return screenDensity;
    }

    float getDayIndicatorRadius() {
        return bigCircleIndicatorRadius;
    }

    void setGrowFactorIndicator(float growfactorIndicator) {
        this.growfactorIndicator = growfactorIndicator;
    }

    float getGrowFactorIndicator() {
        return growfactorIndicator;
    }

    void setAnimationStatus(int animationStatus) {
        this.animationStatus = animationStatus;
    }

    int getTargetHeight() {
        return targetHeight;
    }

    int getWidth() {
        return width;
    }

    void setListener(CompactCalendarViewListener listener) {
        this.listener = listener;
    }

    void removeAllEvents() {
        eventsContainer.removeAllEvents();
    }

    void setFirstDayOfWeek(int day) {
        if (day < 1 || day > 7) {
            throw new IllegalArgumentException(
                    "Day must be an int between 1 and 7 or DAY_OF_WEEK from " +
                            "Java Calendar class. For more information please" +
                            " see Calendar.DAY_OF_WEEK.");
        }
        this.firstDayOfWeekToDraw = day;
        setUseWeekDayAbbreviation(useThreeLetterAbbreviation);
        eventsCalendar.setFirstDayOfWeek(day);
        calendarWithFirstDayOfMonth.setFirstDayOfWeek(day);
        todayCalender.setFirstDayOfWeek(day);
        currentCalender.setFirstDayOfWeek(day);
        tempPreviousMonthCalendar.setFirstDayOfWeek(day);
    }

    void setCurrentSelectedDayBackgroundColor(int currentSelectedDayBackgroundColor) {
        this.currentSelectedDayBackgroundColor = currentSelectedDayBackgroundColor;
    }

    void setCurrentSelectedDayTextColor(int currentSelectedDayTextColor) {
        this.currentSelectedDayTextColor = currentSelectedDayTextColor;
    }

    void setCalenderBackgroundColor(int calenderBackgroundColor) {
        this.calenderBackgroundColor = calenderBackgroundColor;
    }

    void setCurrentDayBackgroundColor(int currentDayBackgroundColor) {
        this.currentDayBackgroundColor = currentDayBackgroundColor;
    }

    void setCurrentDayTextColor(int currentDayTextColor) {
        this.currentDayTextColor = currentDayTextColor;
    }

    void scrollRight() {
        if (isRtl) {
            scrollPrev();
        } else {
            scrollNext();
        }
    }

    void scrollLeft() {
        if (isRtl) {
            scrollNext();
        } else {
            scrollPrev();
        }
    }

    private void scrollNext() {
        monthsScrolledSoFar = monthsScrolledSoFar - 1;
        accumulatedScrollOffset.modify(monthsScrolledSoFar * width, accumulatedScrollOffset.getPointY());
        if (shouldSelectFirstDayOfMonthOnScroll) {
            setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentCalender.getTime(), 0, 1);
            setCurrentDate(calendarWithFirstDayOfMonth.getTime());
        }
        performMonthScrollCallback();
    }

    private void scrollPrev() {
        monthsScrolledSoFar = monthsScrolledSoFar + 1;
        accumulatedScrollOffset.modify(monthsScrolledSoFar * width, accumulatedScrollOffset.getPointY());
        if (shouldSelectFirstDayOfMonthOnScroll) {
            setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentCalender.getTime(), 0, -1);
            setCurrentDate(calendarWithFirstDayOfMonth.getTime());
        }
        performMonthScrollCallback();
    }

    void setLocale(TimeZone timeZone, Locale locale) {
        if (locale == null) {
            throw new IllegalArgumentException("Locale cannot be null.");
        }
        if (timeZone == null) {
            throw new IllegalArgumentException("TimeZone cannot be null.");
        }
        this.locale = locale;
        this.timeZone = timeZone;
        this.eventsContainer = new EventsContainer(Calendar.getInstance(this.timeZone, this.locale));
        // passing null will not re-init density related values - and that's ok
        init(null);
    }

    void setUseWeekDayAbbreviation(boolean useThreeLetterAbbreviation) {
        this.useThreeLetterAbbreviation = useThreeLetterAbbreviation;
        this.dayColumnNames = WeekUtils.getWeekdayNames(locale, firstDayOfWeekToDraw,
                this.useThreeLetterAbbreviation);
    }

    void setDayColumnNames(String[] dayColumnNames) {
        if (dayColumnNames == null || dayColumnNames.length != 7) {
            throw new IllegalArgumentException("Column names cannot be null and must " +
                    "contain a value for each day of the week");
        }
        this.dayColumnNames = dayColumnNames;
    }

    void setShouldDrawDaysHeader(boolean shouldDrawDaysHeader) {
        this.shouldDrawDaysHeader = shouldDrawDaysHeader;
    }

    void onMeasure(int width, int height, int paddingRight, int paddingLeft) {
        widthPerDay = (width) / DAYS_IN_WEEK;
        heightPerDay = targetHeight > 0 ? targetHeight / 7 : height / 7;
        this.width = width;
        this.distanceThresholdForAutoScroll = (int) (width * 0.50);
        this.height = height;
        this.paddingRight = paddingRight;
        this.paddingLeft = paddingLeft;
        // makes easier to find radius
        bigCircleIndicatorRadius = getInterpolatedBigCircleIndicator();
        // scale the selected day indicators slightly so that event indicators can be drawn below
        bigCircleIndicatorRadius = shouldDrawIndicatorsBelowSelectedDays &&
                eventIndicatorStyle == CompactCalendarView.SMALL_INDICATOR ?
                bigCircleIndicatorRadius * 0.85f : bigCircleIndicatorRadius;
    }

    // assume square around each day of width and height = heightPerDay and get diagonal line length
    // interpolate height and radius
    // https://en.wikipedia.org/wiki/Linear_interpolation
    private float getInterpolatedBigCircleIndicator() {
        double x0 = textSizeRect.getHeight();
        double x1 = (heightPerDay + textSizeRect.getHeight()) / 2f;
        double y1 = 0.5 * Math.sqrt((heightPerDay * heightPerDay) + (heightPerDay * heightPerDay));
        double y0 = 0.5 * Math.sqrt((x0 * x0) + (x0 * x0));
        BigDecimal add = BigDecimal.valueOf(y0).add((BigDecimal.valueOf(y1).subtract(BigDecimal.valueOf(y0)))
                .multiply(BigDecimal.valueOf(x1).subtract(BigDecimal.valueOf(x0))
                        .divide(BigDecimal.valueOf(heightPerDay).subtract(BigDecimal.valueOf(x0)), 6, BigDecimal.ROUND_HALF_UP)));
        return add.floatValue();
    }

    void onDraw(Canvas canvas) {
        paddingWidth = widthPerDay / 2;
        paddingHeight = heightPerDay / 2;
        calculateXPositionOffset();
        if (animationStatus == EXPOSE_CALENDAR_ANIMATION) {
            drawCalendarWhileAnimating(canvas);
        } else if (animationStatus == ANIMATE_INDICATORS) {
            drawCalendarWhileAnimatingIndicators(canvas);
        } else {
            drawCalenderBackground(canvas);
            drawScrollableCalender(canvas);
        }
    }

    private void drawCalendarWhileAnimatingIndicators(Canvas canvas) {
        dayPaint.setColor(new Color(calenderBackgroundColor));
        dayPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(0, 0, growFactor, dayPaint);
        dayPaint.setStyle(Paint.Style.STROKE_STYLE);
        dayPaint.setColor(Color.WHITE);
        drawScrollableCalender(canvas);
    }

    private void drawCalendarWhileAnimating(Canvas canvas) {
        background.setColor(new Color(calenderBackgroundColor));
        background.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(0, 0, growFactor, background);
        dayPaint.setStyle(Paint.Style.STROKE_STYLE);
        dayPaint.setColor(Color.WHITE);
        drawScrollableCalender(canvas);
    }

    void onSingleTapUp(Point point) {
        // Don't handle single tap when calendar is scrolling and is not stationary
        if (isScrolling()) {
            return;
        }
        int dayColumn = Math.round((float) (paddingLeft + (double) point.getPointX() -
                paddingWidth - paddingRight) / widthPerDay);
        int dayRow = Math.round(BigDecimal.valueOf(point.getPointY())
                .subtract(BigDecimal.valueOf(paddingHeight))
                .divide(BigDecimal.valueOf(heightPerDay), 6, BigDecimal.ROUND_HALF_UP).floatValue());

        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth,
                currentDate, monthsScrolledSoFar(), 0);

        int firstDayOfMonth = getDayOfWeek(calendarWithFirstDayOfMonth);

        int dayOfMonth = ((dayRow - 1) * 7) - firstDayOfMonth;
        if (isRtl) {
            dayOfMonth += 6 - dayColumn;
        } else {
            dayOfMonth += dayColumn;
        }
        if (dayOfMonth < calendarWithFirstDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH)
                && dayOfMonth >= 0) {
            calendarWithFirstDayOfMonth.add(Calendar.DATE, dayOfMonth);

            currentCalender.setTimeInMillis(calendarWithFirstDayOfMonth.getTimeInMillis());
            performOnDayClickCallback(currentCalender.getTime());
        }
    }

    // Add a little leeway buy checking if amount scrolled is almost same as expected scroll
    // as it maybe off by a few pixels
    private boolean isScrolling() {
        float scrolledX = Math.abs(accumulatedScrollOffset.getPointX());
        int expectedScrollX = Math.abs(width * monthsScrolledSoFar);
        return scrolledX < expectedScrollX - 5 || scrolledX > expectedScrollX + 5;
    }

    private void performOnDayClickCallback(Date date) {
        if (listener != null) {
            listener.onDayClick(date);
        }
    }

    void onScroll(float distanceX, float distanceY) {
        // ignore scrolling callback if already smooth scrolling
        if (isSmoothScrolling) {
            return;
        }
        if (currentDirection == Direction.NONE) {
            if (Math.abs(distanceX) >= Math.abs(distanceY)) {
                currentDirection = Direction.HORIZONTAL;
            } else {
                currentDirection = Direction.VERTICAL;
            }
        }
        isScrolling = true;
        this.distanceX = distanceX;
    }

    void onTouch(TouchEvent event) {
        if (velocityTracker == null) {
            velocityTracker = VelocityDetector.obtainInstance();
        }
        velocityTracker.addEvent(event);
        if (event.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
            if (!scroller.isFinished()) {
                scroller.abortAnimation();
            }
            isSmoothScrolling = false;
        } else if (event.getAction() == TouchEvent.POINT_MOVE) {
            velocityTracker.addEvent(event);
            velocityTracker.calculateCurrentVelocity(500);
        } else if (event.getAction() == TouchEvent.PRIMARY_POINT_UP) {
            handleHorizontalScrolling();
            velocityTracker.clear();
            velocityTracker = null;
            isScrolling = false;
        }
    }

    private void snapBackScroller() {
        float remainingScrollAfterFingerLifted1 = (float) ((double) accumulatedScrollOffset.getPointX()
                - (monthsScrolledSoFar * width));
        scroller.startScroll((int) accumulatedScrollOffset.getPointX(), 0,
                -(int) remainingScrollAfterFingerLifted1, 0);
    }


    private void handleHorizontalScrolling() {
        int velocityX = computeVelocity();
        handleSmoothScrolling(velocityX);

        currentDirection = Direction.NONE;
        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentDate,
                monthsScrolledSoFar(), 0);

        if (calendarWithFirstDayOfMonth.get(Calendar.MONTH) != currentCalender.get(Calendar.MONTH) &&
                shouldSelectFirstDayOfMonthOnScroll) {
            setCalenderToFirstDayOfMonth(currentCalender, currentDate, monthsScrolledSoFar(), 0);
        }
    }

    private int computeVelocity() {
        int maximumVelocity = 24000;
        velocityTracker.calculateCurrentVelocity(VELOCITY_UNIT_PIXELS_PER_SECOND,
                maximumVelocity, maximumVelocity);
        return (int) velocityTracker.getVelocity()[0];
    }

    private void handleSmoothScrolling(int velocityX) {
        int distanceScrolled = (int) ((double) accumulatedScrollOffset.getPointX() - (width * monthsScrolledSoFar));
        boolean isEnoughTimeElapsedSinceLastSmoothScroll = System.currentTimeMillis() -
                lastAutoScrollFromFling > LAST_FLING_THRESHOLD_MILLIS;
        if (velocityX > densityAdjustedSnapVelocity && isEnoughTimeElapsedSinceLastSmoothScroll) {
            scrollPreviousMonth();
        } else if (velocityX < -densityAdjustedSnapVelocity && isEnoughTimeElapsedSinceLastSmoothScroll) {
            scrollNextMonth();
        } else if (isScrolling && distanceScrolled > distanceThresholdForAutoScroll) {
            scrollPreviousMonth();
        } else if (isScrolling && distanceScrolled < -distanceThresholdForAutoScroll) {
            scrollNextMonth();
        } else {
            isSmoothScrolling = false;
            snapBackScroller();
        }
    }

    private void scrollNextMonth() {
        lastAutoScrollFromFling = System.currentTimeMillis();
        monthsScrolledSoFar = monthsScrolledSoFar - 1;
        performScroll();
        isSmoothScrolling = true;
        performMonthScrollCallback();
    }

    private void scrollPreviousMonth() {
        lastAutoScrollFromFling = System.currentTimeMillis();
        monthsScrolledSoFar = monthsScrolledSoFar + 1;
        performScroll();
        isSmoothScrolling = true;
        performMonthScrollCallback();
    }

    private void performMonthScrollCallback() {
        if (listener != null) {
            listener.onMonthScroll(getFirstDayOfCurrentMonth());
        }
    }

    private void performScroll() {
        int targetScroll = (monthsScrolledSoFar) * width;
        double remainingScrollAfterFingerLifted = targetScroll - (double) accumulatedScrollOffset.getPointX();
        scroller.startScroll(accumulatedScrollOffset.getPointXToInt(), 0,
                (int) (remainingScrollAfterFingerLifted), 0);
    }

    int getHeightPerDay() {
        return heightPerDay;
    }

    int getWeekNumberForCurrentMonth() {
        Calendar calendar = Calendar.getInstance(timeZone, locale);
        calendar.setTime(currentDate);
        return calendar.get(Calendar.WEEK_OF_MONTH);
    }

    Date getFirstDayOfCurrentMonth() {
        Calendar calendar = Calendar.getInstance(timeZone, locale);
        calendar.setTime(currentDate);
        calendar.add(Calendar.MONTH, monthsScrolledSoFar());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        setToMidnight(calendar);
        return calendar.getTime();
    }

    void setCurrentDate(Date dateTimeMonth) {
        distanceX = 0;
        monthsScrolledSoFar = 0;
        accumulatedScrollOffset.modify(0, accumulatedScrollOffset.getPointY());
        scroller.startScroll(0, 0, 0, 0);
        currentDate = new Date(dateTimeMonth.getTime());
        currentCalender.setTime(currentDate);
        todayCalender = Calendar.getInstance(timeZone, locale);
        setToMidnight(currentCalender);
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    void addEvent(Event event) {
        eventsContainer.addEvent(event);
    }

    void addEvents(List<Event> events) {
        eventsContainer.addEvents(events);
    }

    List<Event> getCalendarEventsFor(long epochMillis) {
        return eventsContainer.getEventsFor(epochMillis);
    }

    List<Event> getCalendarEventsForMonth(long epochMillis) {
        return eventsContainer.getEventsForMonth(epochMillis);
    }

    void removeEventsFor(long epochMillis) {
        eventsContainer.removeEventByEpochMillis(epochMillis);
    }

    void removeEvent(Event event) {
        eventsContainer.removeEvent(event);
    }

    void removeEvents(List<Event> events) {
        eventsContainer.removeEvents(events);
    }

    void setGrowProgress(float grow) {
        growFactor = grow;
    }

    float getGrowFactor() {
        return growFactor;
    }

    boolean computeScroll() {
        if (scroller.computeScrollOffset()) {
            accumulatedScrollOffset.modify(scroller.getCurrX(),
                    accumulatedScrollOffset.getPointY());
            return true;
        }
        return false;
    }


    private void drawScrollableCalender(Canvas canvas) {
        if (isRtl) {
            drawNextMonth(canvas, -1);
            drawCurrentMonth(canvas);
            drawPreviousMonth(canvas, 1);
        } else {
            drawPreviousMonth(canvas, -1);
            drawCurrentMonth(canvas);
            drawNextMonth(canvas, 1);
        }
    }

    private void drawNextMonth(Canvas canvas, int offset) {
        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentDate, -monthsScrolledSoFar, offset);
        drawMonth(canvas, calendarWithFirstDayOfMonth, (width * (-monthsScrolledSoFar + 1)),
                "drawNextMonth");
    }

    private void drawCurrentMonth(Canvas canvas) {
        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentDate,
                monthsScrolledSoFar(), 0);
        drawMonth(canvas, calendarWithFirstDayOfMonth, width * -monthsScrolledSoFar,
                "drawCurrentMonth");
    }

    private int monthsScrolledSoFar() {
        return isRtl ? monthsScrolledSoFar : -monthsScrolledSoFar;
    }

    private void drawPreviousMonth(Canvas canvas, int offset) {
        setCalenderToFirstDayOfMonth(calendarWithFirstDayOfMonth, currentDate, -monthsScrolledSoFar, offset);
        drawMonth(canvas, calendarWithFirstDayOfMonth, (width * (-monthsScrolledSoFar - 1)),
                "drawPreviousMonth");
    }

    private void calculateXPositionOffset() {
        if (currentDirection == Direction.HORIZONTAL) {
            accumulatedScrollOffset.modify((float) ((double) accumulatedScrollOffset.getPointX() +
                    distanceX), accumulatedScrollOffset.getPointY());
        }
    }

    private void drawCalenderBackground(Canvas canvas) {
        dayPaint.setColor(new Color(calenderBackgroundColor));
        dayPaint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawRect(0, 0, width, height, dayPaint);
        dayPaint.setStyle(Paint.Style.STROKE_STYLE);
        dayPaint.setColor(new Color(calenderTextColor));
    }

    void drawEvents(Canvas canvas, Calendar currentMonthToDrawCalender, int offset, String from) {
        if (from == null) {
            currentMonthToDrawCalender.get(Calendar.MONTH);
        }
        int currentMonth = currentMonthToDrawCalender.get(Calendar.MONTH);
        List<Events> uniqEvents = eventsContainer.getEventsForMonthAndYear(currentMonth,
                currentMonthToDrawCalender.get(Calendar.YEAR));
        boolean shouldDrawCurrentDayCircle = currentMonth == todayCalender.get(Calendar.MONTH);
        boolean shouldDrawSelectedDayCircle = currentMonth == currentCalender.get(Calendar.MONTH);
        int todayDayOfMonth = todayCalender.get(Calendar.DAY_OF_MONTH);
        int currentYear = todayCalender.get(Calendar.YEAR);
        int selectedDayOfMonth = currentCalender.get(Calendar.DAY_OF_MONTH);
        float indicatorOffset = bigCircleIndicatorRadius / 2;
        if (uniqEvents != null) {
            for (Events events : uniqEvents) {
                long timeMillis = events.getTimeInMillis();
                eventsCalendar.setTimeInMillis(timeMillis);
                int dayOfWeek = getDayOfWeek(eventsCalendar);
                if (isRtl) {
                    dayOfWeek = 6 - dayOfWeek;
                }
                int weekNumberForMonth = eventsCalendar.get(Calendar.WEEK_OF_MONTH);
                float xPosition = (float) (widthPerDay * dayOfWeek + paddingWidth + paddingLeft +
                        (double) accumulatedScrollOffset.getPointX() + offset - paddingRight);
                float yPosition = weekNumberForMonth * heightPerDay + paddingHeight;
                if (((animationStatus == EXPOSE_CALENDAR_ANIMATION || animationStatus ==
                        ANIMATE_INDICATORS) && xPosition >= growFactor) || yPosition >= growFactor) {
                    // only draw small event indicators if enough of the calendar is exposed
                    continue;
                } else if (animationStatus == EXPAND_COLLAPSE_CALENDAR && yPosition >= growFactor) {
                    // expanding animation, just draw event indicators if enough of the calendar is visible
                    continue;
                } else if (animationStatus == EXPOSE_CALENDAR_ANIMATION &&
                        (eventIndicatorStyle == FILL_LARGE_INDICATOR ||
                                eventIndicatorStyle == NO_FILL_LARGE_INDICATOR)) {
                    // Don't draw large indicators during expose animation, until animation is done
                    continue;
                }
                List<Event> eventsList = events.getEvents();
                int dayOfMonth = eventsCalendar.get(Calendar.DAY_OF_MONTH);
                int eventYear = eventsCalendar.get(Calendar.YEAR);
                boolean isSameDayAsCurrentDay = shouldDrawCurrentDayCircle &&
                        (todayDayOfMonth == dayOfMonth) && (eventYear == currentYear);
                boolean isCurrentSelectedDay = shouldDrawSelectedDayCircle &&
                        (selectedDayOfMonth == dayOfMonth);
                if (shouldDrawIndicatorsBelowSelectedDays || !isSameDayAsCurrentDay &&
                        !isCurrentSelectedDay || animationStatus == EXPOSE_CALENDAR_ANIMATION) {
                    if (eventIndicatorStyle == FILL_LARGE_INDICATOR || eventIndicatorStyle ==
                            NO_FILL_LARGE_INDICATOR) {
                        if (!eventsList.isEmpty()) {
                            Event event = eventsList.get(0);
                            drawEventIndicatorCircle(canvas, xPosition, yPosition, event.getColor());
                        }
                    } else {
                        yPosition = BigDecimal.valueOf(yPosition).add(BigDecimal.valueOf(indicatorOffset)).floatValue();
                        // offset event indicators to draw below selected day indicators
                        // this makes sure that they do no overlap
                        if (shouldDrawIndicatorsBelowSelectedDays && (isSameDayAsCurrentDay ||
                                isCurrentSelectedDay)) {
                            yPosition = BigDecimal.valueOf(yPosition).add(BigDecimal.valueOf(indicatorOffset)).floatValue();
                        }
                        if (eventsList.size() >= 3) {
                            drawEventsWithPlus(canvas, xPosition, yPosition, eventsList);
                        } else if (eventsList.size() == 2) {
                            drawTwoEvents(canvas, xPosition, yPosition, eventsList);
                        } else if (eventsList.size() == 1) {
                            drawSingleEvent(canvas, xPosition, yPosition, eventsList);
                        }
                    }
                }
            }
        }
    }

    private void drawSingleEvent(Canvas canvas, float xPosition, float yPosition, List<Event> eventsList) {
        Event event = eventsList.get(0);
        drawEventIndicatorCircle(canvas, xPosition, yPosition, event.getColor());
    }

    private void drawTwoEvents(Canvas canvas, float xPosition, float yPosition, List<Event> eventsList) {
        // draw fist event just left of center
        drawEventIndicatorCircle(canvas, BigDecimal.valueOf(xPosition).add(BigDecimal.valueOf(xIndicatorOffset)
                        .multiply(BigDecimal.valueOf(-1))).floatValue(),
                yPosition, eventsList.get(0).getColor());
        // draw second event just right of center
        drawEventIndicatorCircle(canvas, BigDecimal.valueOf(xPosition).add(BigDecimal.valueOf(xIndicatorOffset))
                .floatValue(), yPosition, eventsList.get(1).getColor());
    }

    // draw 2 eventsByMonthAndYearMap followed by plus indicator to show
    // there are more than 2 eventsByMonthAndYearMap
    private void drawEventsWithPlus(Canvas canvas, float xPosition,
                                    float yPosition, List<Event> eventsList) {
        // k = size() - 1, but since we don't want to draw more than 2 indicators,
        // we just stop after 2 iterations so we can just hard k = -2 instead
        // we can use the below loop to draw arbitrary eventsByMonthAndYearMap
        // based on the current screen size, for example, larger screens should be able to
        // display more than 2 evens before displaying plus indicator, but don't
        // draw more than 3 indicators for now
        for (int j = 0, k = -2; j < 3; j++, k += 2) {
            Event event = eventsList.get(j);
            float xStartPosition = BigDecimal.valueOf(xPosition).add(BigDecimal.valueOf(xIndicatorOffset).multiply(BigDecimal.valueOf(k))).floatValue();
            if (j == 2) {
                dayPaint.setColor(new Color(multiEventIndicatorColor));
                dayPaint.setStrokeWidth(1);
                canvas.drawLine(BigDecimal.valueOf(xStartPosition).subtract(BigDecimal.valueOf(smallIndicatorRadius)).floatValue(), yPosition,
                        BigDecimal.valueOf(xStartPosition).add(BigDecimal.valueOf(smallIndicatorRadius)).floatValue(), yPosition, dayPaint);
                canvas.drawLine(xStartPosition, BigDecimal.valueOf(yPosition)
                                .subtract(BigDecimal.valueOf(smallIndicatorRadius)).floatValue(),
                        xStartPosition, BigDecimal.valueOf(yPosition)
                                .add(BigDecimal.valueOf(smallIndicatorRadius)).floatValue(), dayPaint);
                dayPaint.setStrokeWidth(0);
            } else {
                drawEventIndicatorCircle(canvas, xStartPosition, yPosition, event.getColor());
            }
        }
    }

    // zero based indexes used internally so instead of returning range of 1-7 like calendar class
    // it returns 0-6 where 0 is Sunday instead of 1
    int getDayOfWeek(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - firstDayOfWeekToDraw;
        dayOfWeek = dayOfWeek < 0 ? 7 + dayOfWeek : dayOfWeek;
        return dayOfWeek;
    }

    void drawMonth(Canvas canvas, Calendar monthToDrawCalender, int offset, String from) {
        drawEvents(canvas, monthToDrawCalender, offset, from);
        // offset by one because we want to start from Monday
        int firstDayOfMonth = getDayOfWeek(monthToDrawCalender);
        tempPreviousMonthCalendar.setTimeInMillis(monthToDrawCalender.getTimeInMillis());
        tempPreviousMonthCalendar.add(Calendar.MONTH, -1);
        for (int dayColumn = 0, colDirection = isRtl ? 6 : 0, dayRow = 0; dayColumn <= 6; dayRow++) {
            if (dayRow == 7) {
                if (isRtl) {
                    colDirection--;
                } else {
                    colDirection++;
                }
                dayRow = 0;
                dayColumn++;
            }
            if (dayColumn == dayColumnNames.length) {
                break;
            }
            float xPosition = (float) (widthPerDay * dayColumn + paddingWidth + paddingLeft +
                    (double) accumulatedScrollOffset.getPointX() + offset - paddingRight);
            float yPosition = dayRow * heightPerDay + paddingHeight;
            boolean isAnimatingWithExpose = animationStatus == EXPOSE_CALENDAR_ANIMATION;
            if (xPosition >= growFactor && (isAnimatingWithExpose || animationStatus ==
                    ANIMATE_INDICATORS) || yPosition >= growFactor) {
                // don't draw days if animating expose or indicators
                continue;
            }
            if (dayRow == 0) {
                // first row, so draw the first letter of the day
                if (shouldDrawDaysHeader) {
                    dayPaint.setColor(new Color(calenderTextColor));
                    dayPaint.setFont(Font.DEFAULT_BOLD);
                    dayPaint.setStyle(Paint.Style.FILL_STYLE);
                    dayPaint.setColor(new Color(calenderTextColor));
                    canvas.drawText(dayPaint, dayColumnNames[colDirection], xPosition, paddingHeight);
                    dayPaint.setFont(Font.DEFAULT);
                }
            } else {
                float value = BigDecimal.valueOf(yPosition).subtract(BigDecimal.valueOf(12)).floatValue();
                int day = ((dayRow - 1) * 7 + colDirection + 1) - firstDayOfMonth;
                int defaultCalenderTextColorToUse = calenderTextColor;
                boolean isSameMonthAsToday = monthToDrawCalender.get(Calendar.MONTH) ==
                        todayCalender.get(Calendar.MONTH);
                boolean isSameYearAsToday = monthToDrawCalender.get(Calendar.YEAR) ==
                        todayCalender.get(Calendar.YEAR);
                boolean isSameMonthAsCurrentCalendar = monthToDrawCalender.get(Calendar.MONTH) ==
                        currentCalender.get(Calendar.MONTH) &&
                        monthToDrawCalender.get(Calendar.YEAR) == currentCalender.get(Calendar.YEAR);
                int todayDayOfMonth = todayCalender.get(Calendar.DAY_OF_MONTH);
                if (currentCalender.get(Calendar.DAY_OF_MONTH) == day && isSameMonthAsCurrentCalendar
                        && !isAnimatingWithExpose) {
                    drawDayCircleIndicator(currentSelectedDayIndicatorStyle, canvas, xPosition,
                            value, currentSelectedDayBackgroundColor);
                    defaultCalenderTextColorToUse = currentSelectedDayTextColor;
                } else if (isSameYearAsToday && isSameMonthAsToday &&
                        todayDayOfMonth == day && !isAnimatingWithExpose) {
                    drawDayCircleIndicator(currentDayIndicatorStyle, canvas, xPosition,
                            value, currentDayBackgroundColor);
                    defaultCalenderTextColorToUse = currentDayTextColor;
                }
                int maximumMonthDay = monthToDrawCalender.getActualMaximum(Calendar.DAY_OF_MONTH);
                if (day <= 0) {
                    if (displayOtherMonthDays) {
                        int maximumPreviousMonthDay = tempPreviousMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                        // Display day month before
                        dayPaint.setStyle(Paint.Style.FILL_STYLE);
                        dayPaint.setColor(new Color(otherMonthDaysTextColor));
                        canvas.drawText(dayPaint, String.valueOf(maximumPreviousMonthDay + day),
                                xPosition, yPosition);
                    }
                } else if (day > maximumMonthDay) {
                    if (displayOtherMonthDays) {
                        // Display day month after
                        dayPaint.setStyle(Paint.Style.FILL_STYLE);
                        dayPaint.setColor(new Color(otherMonthDaysTextColor));
                        canvas.drawText(dayPaint, String.valueOf(day - maximumMonthDay),
                                xPosition, yPosition);
                    }
                } else {
                    dayPaint.setStyle(Paint.Style.FILL_STYLE);
                    dayPaint.setColor(new Color(defaultCalenderTextColorToUse));
                    canvas.drawText(dayPaint, String.valueOf(day), xPosition, yPosition);
                }
            }
        }
    }

    private void drawDayCircleIndicator(int indicatorStyle, Canvas canvas, float xP, float yP, int color) {
        drawDayCircleIndicator(indicatorStyle, canvas, xP, yP, color, 1);
    }

    private void drawDayCircleIndicator(int indicatorStyle, Canvas canvas,
                                        float xP, float yP, int color, float circleScale) {
        float strokeWidth = dayPaint.getStrokeWidth();
        if (indicatorStyle == NO_FILL_LARGE_INDICATOR) {
            dayPaint.setStrokeWidth(2 * screenDensity);
            dayPaint.setStyle(Paint.Style.STROKE_STYLE);
        } else {
            dayPaint.setStyle(Paint.Style.FILL_STYLE);
        }
        drawCircle(canvas, xP, yP, color, circleScale);
        dayPaint.setStrokeWidth(strokeWidth);
        dayPaint.setStyle(Paint.Style.FILL_STYLE);
    }

    // Draw Circle on certain days to highlight them
    private void drawCircle(Canvas canvas, float xP, float yP, int color, float circleScale) {
        dayPaint.setColor(new Color(color));
        if (animationStatus == ANIMATE_INDICATORS) {
            float maxRadius = circleScale * bigCircleIndicatorRadius * 1.4f;
            drawCircle(canvas, Math.min(growfactorIndicator, maxRadius), xP,
                    BigDecimal.valueOf(yP).subtract(BigDecimal.valueOf(textHeight / 6)).floatValue());
        } else {
            drawCircle(canvas, circleScale * bigCircleIndicatorRadius, xP,
                    BigDecimal.valueOf(yP).subtract(BigDecimal.valueOf(textHeight / 6)).floatValue());
        }
    }

    private void drawEventIndicatorCircle(Canvas canvas, float xP, float yP, int color) {
        dayPaint.setColor(new Color(color));
        if (eventIndicatorStyle == SMALL_INDICATOR) {
            dayPaint.setStyle(Paint.Style.FILL_STYLE);
            drawCircle(canvas, smallIndicatorRadius, xP, yP);
        } else if (eventIndicatorStyle == NO_FILL_LARGE_INDICATOR) {
            dayPaint.setStyle(Paint.Style.STROKE_STYLE);
            drawDayCircleIndicator(NO_FILL_LARGE_INDICATOR, canvas, xP, yP, color);
        } else if (eventIndicatorStyle == FILL_LARGE_INDICATOR) {
            drawDayCircleIndicator(FILL_LARGE_INDICATOR, canvas, xP, yP, color);
        }
    }

    private void drawCircle(Canvas canvas, float radius, float xP, float yP) {
        canvas.drawCircle(xP, yP, radius, dayPaint);
    }
}
